#!/bin/bash

echo 'These iptables, ip6tables and modprobe binaries are now specified correctly for Ubuntu 20.04 and Debian'
echo 'To locate the binaries on your system, type the following commands'
echo 'which iptables'
echo 'which ip6tables'
echo 'which modprobe'
IPT4='/usr/sbin/iptables'
IPT6='/usr/sbin/ip6tables'
Modprob='/usr/sbin/modprobe'

InetIF='ens18'
InetIP='217.197.116.89'
LocalNet=(192.168.115.0/24 192.168.116.0/24)

echo 'You can specify the ports, separated by commas, as they are currently listed for SSH'
PortSSH='22,32852'
PortVPN='443'

echo 'Сброс всех параметров iptables IPv4'
$IPT4 -F
$IPT4 -X
$IPT4 -t nat -F
$IPT4 -t nat -X
$IPT4 -t mangle -F
$IPT4 -t mangle -X
$IPT4 -t raw -F
$IPT4 -t raw -X

$IPT4 -P INPUT ACCEPT
$IPT4 -P FORWARD ACCEPT
$IPT4 -P OUTPUT ACCEPT

echo 'Сброс всех параметров iptables IPv6'
$IPT6 -F
$IPT6 -X
$IPT6 -t nat -F
$IPT6 -t nat -X
$IPT6 -t mangle -F
$IPT6 -t mangle -X
$IPT6 -t raw -F
$IPT6 -t raw -X

$IPT6 -P INPUT DROP
$IPT6 -P FORWARD DROP
$IPT6 -P OUTPUT DROP

if [[ $1 == stop ]]; then
  exit 0
fi

echo; echo 'Настройки firewall'
echo 'Включаем модули ядра'
$Modprob nf_conntrack

echo 'Блокировка плохого трафика'
$IPT4 -t raw -A PREROUTING -p tcp --tcp-flags ALL NONE -m comment --comment "Блокируем входящие нулевые пакеты" -j DROP
$IPT4 -t mangle -A PREROUTING -m conntrack --ctstate INVALID -m comment --comment "Блокируем входящие не идентифицированные пакеты" -j DROP

echo 'Разрешаем всё что уже получило разрешение для входящего трафика'
$IPT4 -A INPUT -m conntrack --ctstate UNTRACKED -m comment --comment "Разрешаем весь неотслеживаемый входящий трафик" -j ACCEPT
$IPT4 -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -m comment --comment "Разрешим уже разрешённый, входящий трафик." -j ACCEPT

echo 'Разрешаем всё что уже получило разрешение для проходящего трафика'
$IPT4 -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -m comment --comment "Разрешим уже разрешённый, проходящий трафик" -j ACCEPT

echo 'Отключаем отслеживание пакетов от localhost'
$IPT4 -t raw -A PREROUTING -i lo -d 127.0.0.1 -m comment --comment "Не будем отслеживать локальные запросы." -j CT --notrack
$IPT4 -t raw -A OUTPUT -o lo -d 127.0.0.1 -m comment --comment "Не будем отслеживать локальные запросы." -j CT --notrack

echo 'Разрешаем ICMP (ping) и IGMP'
$IPT4 -A INPUT -p igmp -m comment --comment "Разрешим проходящие igmp пакеты" -j ACCEPT
$IPT4 -A INPUT -p icmp --icmp-type echo-request -m comment --comment "Разрешим входящие ICMP (echo-request)" -j ACCEPT

echo 'Разрешаем SSH порт'
$IPT4 -A INPUT -p tcp -m multiport --dport $PortSSH -m comment --comment "Разрешаем SSH порт" -j ACCEPT

for UserNet in ${LocalNet[@]}; do
  echo 'Выравнивание MTU'
  $IPT4 -t mangle -A POSTROUTING -s $UserNet -o $InetIF -p tcp --tcp-flags SYN,RST SYN -m comment --comment "Выравнивание MTU" -j TCPMSS --clamp-mss-to-pmtu

  echo 'Отключаем отслеживание пакетов для OpenConnect'
  $IPT4 -t raw -A PREROUTING -i $InetIF -d $InetIP -p tcp -m multiport --dport $PortVPN -m comment --comment "Отключаем отслеживание пакетов для OpenConnect" -j CT --notrack

  echo 'Разрешаем интернет для VPN клиентов'
  $IPT4 -A FORWARD -s $UserNet -o $InetIF -m comment --comment "Разрешаем интернет для VPN клиентов" -j ACCEPT

  echo 'Включаем SNAT для VPN клиентов'
  $IPT4 -t nat -A POSTROUTING -s $UserNet -o $InetIF -m comment --comment "Включаем SNAT для VPN клиентов" -j SNAT --to-source $InetIP
done

echo 'Запрещаем всё остальное входящие/проходящие'
echo 'На Debian (INPUT DROP) точно работает нормально'
echo 'На Ubuntu возможно придётся указать (INPUT ACCEPT) но это не точно'
$IPT4 -P INPUT DROP
$IPT4 -P FORWARD DROP
